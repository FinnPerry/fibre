﻿using UnityEngine;

namespace Fibre
{
    /// <summary>
    /// An event type that specifies a single "next" event in the sequence.
    /// Most event types likely derive from this one.
    /// </summary>
    [CreateAssetMenu(menuName = "Fibre/LinearEvent")]
    public class LinearEvent : FibreEvent
    {
        public FibreEvent NextEvent;

        public override FibreEvent GetNextEvent()
        {
            return NextEvent;
        }
    }
}
