using UnityEngine;

namespace Fibre
{
    /// <summary>
    /// An event type for pausing event progress for a duration in seconds.
    /// </summary>
    [CreateAssetMenu(menuName = "Fibre/SleepEvent")]
    public class SleepEvent : LinearEvent
    {
        public double Duration = 0.0;
        private double _start;

        public override void StartEvent()
        {
            base.StartEvent();
            _start = Time.timeAsDouble;
        }

        public override bool IsInProgress()
        {
            bool passed = HasDurationPassed(_start, Duration, Time.timeAsDouble);
            return base.IsInProgress() || !passed;
        }

        public override void InstantComplete()
        {
            base.InstantComplete();
            _start -= Duration;
        }

        /// <summary>
        /// Check if a duration has passed.
        /// </summary>
        /// <param name="start">The start time.</param>
        /// <param name="dur">Length of the duration.</param>
        /// <param name="time">The current time.</param>
        /// <returns></returns>
        public static bool HasDurationPassed(double start, double dur, double time)
        {
            double elapsed = time - start;
            return elapsed >= dur;
        }
    }
}
