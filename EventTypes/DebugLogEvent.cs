using UnityEngine;

namespace Fibre
{
    /// <summary>
    /// An event type that logs a message to Unity console.
    /// </summary>
    [CreateAssetMenu(menuName = "Fibre/DebugLogEvent")]
    public class DebugLogEvent : LinearEvent
    {
        public string Message = "";

        public override void StartEvent()
        {
            base.StartEvent();
            Debug.Log(Message);
        }
    }
}
