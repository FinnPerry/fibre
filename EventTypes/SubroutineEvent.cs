using Fibre;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An event type for running a "subroutine" event list before resuming the
/// parent event list.
/// </summary>
[CreateAssetMenu(menuName = "Fibre/SubroutineEvent")]
public class SubroutineEvent : LinearEvent
{
    public List<FibreEvent> FirstEvents = new List<FibreEvent>();

    private FibreManager _manager = null;
    private bool _started = false;

    public override void StartEvent()
    {
        base.StartEvent();
        // Create a new manager object.
        var obj = new GameObject("FibreSubroutineManager");
        _manager = obj.AddComponent<FibreManager>();
        _manager.FirstEvents = FirstEvents;
        _manager.DestroyGameObjectOnFinish = true;
        _manager.OnStart += OnSubManagerStart;
    }

    public override bool IsInProgress()
    {
        // Finish when all heads have completed in the sub manager.
        bool inProgress = !_started || _manager.HeadEventCount() > 0;
        return base.IsInProgress() || inProgress;
    }

    public override void InstantComplete()
    {
        base.InstantComplete();
        _manager.InstantComplete();
    }

    /// <summary>
    /// A callback that gets run once the spawned event manager starts running.
    /// </summary>
    private void OnSubManagerStart()
    {
        _started = true;
        _manager.OnStart -= OnSubManagerStart;
    }
}
