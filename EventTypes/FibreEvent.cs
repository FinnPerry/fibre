using UnityEngine;

namespace Fibre
{
    /// <summary>
    /// A base event class that does nothing but provides empty virtual
    /// functions to be overridden.
    /// </summary>
    [CreateAssetMenu(menuName = "Fibre/FibreEvent")]
    public class FibreEvent : ScriptableObject
    {
        public bool Blocking = true;

        /// <summary>
        /// Start running the event.
        /// </summary>
        public virtual void StartEvent() { }

        /// <summary>
        /// Get if the event is currently in progress.
        /// </summary>
        /// <returns>True if the event is in progress.</returns>
        public virtual bool IsInProgress()
        {
            return false;
        }

        /// <summary>
        /// Called when the manager ends the event.
        /// </summary>
        public virtual void OnEventEnd() { }

        /// <summary>
        /// Get the next event to run.
        /// </summary>
        /// <returns>The next event.</returns>
        public virtual FibreEvent GetNextEvent()
        {
            return null;
        }

        /// <summary>
        /// Instantly completete the event.
        /// Tries to place the event in a state where the next call to IsInProgress returns false.
        /// If your event cannot be instantly completed for whatever reason then
        /// simply don't implement this function.
        /// </summary>
        public virtual void InstantComplete() { }

        /// <summary>
        /// Creates a clone of the event.
        /// </summary>
        /// <returns></returns>
        public virtual FibreEvent Clone()
        {
            return Instantiate(this);
        }
    }
}
