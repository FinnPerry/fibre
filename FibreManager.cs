using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fibre
{
    /// <summary>
    /// A script that manages running through a chain of events.
    /// </summary>
    public class FibreManager : MonoBehaviour
    {
        public List<FibreEvent> FirstEvents = new List<FibreEvent>();
        public uint MaxEventsPerFrame = 100;
        public bool DestroyGameObjectOnFinish = false;

        public event Action OnStart = delegate { };

        private readonly List<RunningEvent> _heads = new List<RunningEvent>();
        private List<RunningEvent> _runningEvents = new List<RunningEvent>();
        private bool _shouldInstantCompleteCurrent = false;
        private bool _shouldInstantCompleteNew = false;

        /// <summary>
        /// Start the manager.
        /// Events still running from a previous start are dropped.
        /// </summary>
        public void Start()
        {
            _runningEvents.Clear();
            StartStartingEvents();
        }

        /// <summary>
        /// Progress events, and instantly complete if needed.
        /// Disable the manager if errors occur.
        /// </summary>
        public void Update()
        {
            TryInstantComplete();
            ProgressEvents();
            CleanRunningEvents();
            DestroyIfNeeded();
            _shouldInstantCompleteNew = false;
        }

        /// <summary>
        /// Flag the currently running events to be instantly completed next Update.
        /// </summary>
        public void InstantComplete()
        {
            _shouldInstantCompleteCurrent = true;
        }

        /// <summary>
        /// Flag the currently running events, and all new events started next Update to be instantly completed.
        /// </summary>
        public void InstantCompleteAll()
        {
            InstantComplete();
            _shouldInstantCompleteNew = true;
        }

        /// <summary>
        /// Get the amount of running events which are currently blocking
        /// the event chain progress.
        /// </summary>
        /// <returns></returns>
        public int HeadEventCount()
        {
            return _heads.Count;
        }

        /// <summary>
        /// Get the total amount of running events.
        /// </summary>
        /// <returns></returns>
        public int TotalEventCount()
        {
            return _runningEvents.Count;
        }

        /// <summary>
        /// Start running all the starting events.
        /// </summary>
        private void StartStartingEvents()
        {
            foreach (FibreEvent e in FirstEvents)
            {
                StartEvent(new RunningEvent(e));
            }
            OnStart();
        }

        /// <summary>
        /// Start running an event.
        /// </summary>
        /// <param name="e"></param>
        /// <returns>True on success.</returns>
        private bool StartEvent(RunningEvent e)
        {
            bool success = e.TryStartEvent();
            if (success)
            {
                if (_shouldInstantCompleteNew)
                {
                    e.TryInstantComplete();
                }

                _heads.Add(e);
                _runningEvents.Add(e);
            }
            return success;
        }

        /// <summary>
        /// Replace a head with it's next event.
        /// Ends the head if it is blocking.
        /// </summary>
        /// <param name="curr">The current head to replace.</param>
        /// <param name="newHead">The new head created by this fn.</param>
        /// <returns>True on success.</returns>
        private bool ReplaceHead(RunningEvent curr, out RunningEvent newHead)
        {
            // Get the next event and end the current event if blocking.
            if (
                curr.TryGetNextEvent(out RunningEvent next)
                && (!curr.Blocking || curr.TryEndEvent())
            )
            {
                // Remove the current head.
                _heads.Remove(curr);
                // Start the next event.
                newHead = next;
                return (next != null) ? StartEvent(next) : true;
            }
            newHead = null;
            return false;
        }

        /// <summary>
        /// Instantly complete running events if the flag has been set.
        /// Sets the flag to false.
        /// </summary>
        private void TryInstantComplete()
        {
            if (!_shouldInstantCompleteCurrent)
            {
                return;
            }
            _shouldInstantCompleteCurrent = false;

            // Loop through running events and instantly complete them.
            // If any errors occur or an event fails to instantly
            // complete then the loop exits early.
            foreach (RunningEvent e in _runningEvents)
            {
                e.TryInstantComplete();
            }
        }

        /// <summary>
        /// Progress all heads through the event chain.
        /// </summary>
        private void ProgressEvents()
        {
            // Convert to array because the list may be modified during the loop.
            foreach (RunningEvent head in _heads.ToArray())
            {
                ProgressHead(head);
            }
        }

        /// <summary>
        /// Progress a head through the event chain until an event blocks.
        /// </summary>
        /// <param name="head"></param>
        /// <returns>True on success.</returns>
        private bool ProgressHead(RunningEvent head)
        {
            bool success = true;
            for (uint i = 0; i < MaxEventsPerFrame; ++i)
            {
                // Check if the current event is still in progress.
                if (!head.TryShouldContinue(out bool shouldContinue))
                {
                    success = false;
                    break;
                }

                // Go to the next event.
                if (shouldContinue)
                {
                    if (!ReplaceHead(head, out head))
                    {
                        success = false;
                        break;
                    }

                    // Break if next event is null.
                    if (head == null)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            return success;
        }

        /// <summary>
        /// Remove completed events from the running list.
        /// Events that throw an error are also removed.
        /// </summary>
        private void CleanRunningEvents()
        {
            var newList = new List<RunningEvent>();
            foreach (RunningEvent e in _runningEvents)
            {
                if (e.TryIsInProgress(out bool inProg) && inProg)
                {
                    newList.Add(e);
                }
                else
                {
                    e.TryEndEvent();
                }
            }
            _runningEvents = newList;
        }

        /// <summary>
        /// If the destroy flag is set, destroy the manager object once
        /// all events are complete.
        /// </summary>
        private void DestroyIfNeeded()
        {
            if (DestroyGameObjectOnFinish && TotalEventCount() == 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
