#if UNITY_EDITOR
using System;

namespace Fibre
{
    /// <summary>
    /// An event type that invokes callbacks on each of it's event functions.
    /// </summary>
    public class TestEvent : LinearEvent
    {
        public Action OnStartEvent = null;
        public Action OnIsInProgress = null;
        public Action OnEndEvent = null;
        public Action OnGetNextEvent = null;
        public Action OnInstantComplete = null;
        public Action OnOnClone = null;

        public bool Running = false;
        private bool _skipped = false;

        public override void StartEvent()
        {
            base.StartEvent();
            OnStartEvent?.Invoke();
        }

        public override bool IsInProgress()
        {
            OnIsInProgress?.Invoke();
            return base.IsInProgress() || (Running && !_skipped);
        }

        public override void OnEventEnd()
        {
            OnEndEvent?.Invoke();
            base.OnEventEnd();
        }

        public override FibreEvent GetNextEvent()
        {
            OnGetNextEvent?.Invoke();
            return base.GetNextEvent();
        }

        public override void InstantComplete()
        {
            OnInstantComplete?.Invoke();
            base.InstantComplete();
            _skipped = true;
        }

        public override FibreEvent Clone()
        {
            var clone = base.Clone() as TestEvent;
            // `Action`s are not serialized by Unity so we need to manually
            // copy them to the new instance.
            clone.OnStartEvent = OnStartEvent;
            clone.OnIsInProgress = OnIsInProgress;
            clone.OnEndEvent = OnEndEvent;
            clone.OnGetNextEvent = OnGetNextEvent;
            clone.OnInstantComplete = OnInstantComplete;
            clone.OnOnClone = OnOnClone;
            return clone;
        }
    }
}
#endif
