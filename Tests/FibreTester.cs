#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;

namespace Fibre
{
    /// <summary>
    /// A script that runs Fibre tests on Start.
    /// </summary>
    public class FibreTester : MonoBehaviour
    {
        /// <summary>
        /// Run the tests and log results.
        /// </summary>
        private void Start()
        {
            (int success, int total) = RunTests();
            FibreDebug.Log($"{success}/{total} tests passed. {total - success} failed.");
        }

        /// <summary>
        /// Get a list of test fns.
        /// </summary>
        /// <returns></returns>
        private List<MethodInfo> GetTests()
        {
            var tests = new List<MethodInfo>();
            foreach (MethodInfo method in typeof(FibreTester).GetMethods())
            {
                if (IsTestMethod(method))
                {
                    tests.Add(method);
                }
            }
            return tests;
        }

        /// <summary>
        /// Check if a method is a test method.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private bool IsTestMethod(MethodInfo info)
        {
            bool hasPrefix = info.Name.StartsWith("Test");
            bool returnsVoid = info.ReturnType == typeof(void);
            bool noArgs = info.GetParameters().Length == 0;
            return hasPrefix && returnsVoid && noArgs;
        }

        /// <summary>
        /// Run the tests.
        /// </summary>
        /// <returns>Tuple (success count, total count).</returns>
        private (int, int) RunTests()
        {
            List<MethodInfo> tests = GetTests();
            int success = 0;
            foreach (MethodInfo test in tests)
            {
                try
                {
                    test.Invoke(this, null);
                    ++success;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
            return (success, tests.Count);
        }

        #region MANAGER_TESTS
        /// <summary>
        /// Test that FibreManager starts starting events
        /// </summary>
        public void TestManagerStartsStartingEvents()
        {
            var e0 = ScriptableObject.CreateInstance<TestEvent>();
            bool e0Started = false;
            e0.OnStartEvent = () => e0Started = true;

            var e1 = ScriptableObject.CreateInstance<TestEvent>();
            bool e1Started = false;
            e1.OnStartEvent = () => e1Started = true;

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e0, e1 };
            m.Start();

            Assert.IsTrue(e0Started);
            Assert.IsTrue(e1Started);
        }

        /// <summary>
        /// Test that FibreManager is enabled after successfully starting events
        /// </summary>
        public void TestManagerStartRemainEnabled()
        {
            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.Start();
            Assert.IsTrue(m.enabled);
        }

        /// <summary>
        /// Test that FibreManager remains enabled after successfully updating.
        /// </summary>
        public void TestManagerUpdateRemainEnabled()
        {
            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { ScriptableObject.CreateInstance<TestEvent>() };
            m.Start();
            m.Update();
            Assert.IsTrue(m.enabled);
        }

        /// <summary>
        /// Test that FibreManager remains enabled on update when a non blocking event fails to end.
        /// </summary>
        public void TestManagerUpdateRemainEnabledOnFailedNonBlockingEventEnd()
        {
            var e = ScriptableObject.CreateInstance<TestEvent>();
            e.Blocking = false;
            e.OnEndEvent = () => throw new Exception();

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e };

            m.Start();
            Assert.IsTrue(m.enabled);
            m.Update();
            Assert.IsTrue(m.enabled);
        }

        /// <summary>
        /// Test that FibreManager runs no more than the max amount of events per update.
        /// </summary>
        public void TestManagerMaxEventsPerFrame()
        {
            int count = 0;
            var e = ScriptableObject.CreateInstance<TestEvent>();
            e.NextEvent = e;
            e.OnStartEvent = () => ++count;

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e };
            m.MaxEventsPerFrame = 2;

            m.Start();
            Assert.AreEqual(1, count);
            m.Update();
            Assert.AreEqual(3, count);
            m.Update();
            Assert.AreEqual(5, count);
        }

        /// <summary>
        /// Test that blocking events are ended before the next event starts.
        /// </summary>
        public void TestBlockingEventEndBeforeNextStart()
        {
            var timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            var e0 = ScriptableObject.CreateInstance<TestEvent>();
            long e0EndTime = -1;
            e0.OnEndEvent = () => e0EndTime = timer.ElapsedTicks;

            var e1 = ScriptableObject.CreateInstance<TestEvent>();
            e0.NextEvent = e1;
            long e1StartTime = -1;
            e1.OnStartEvent = () => e1StartTime = timer.ElapsedTicks;

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e0 };
            m.Start();
            m.Update();

            Assert.AreNotEqual(e0EndTime, -1);
            Assert.AreNotEqual(e1StartTime, -1);

            Assert.IsTrue(e1StartTime > e0EndTime);
        }

        /// <summary>
        /// Test that non blocking events continue running when the next event starts.
        /// </summary>
        public void TestNonBlockingRemainsRunningDuringNextEvents()
        {
            var e0 = ScriptableObject.CreateInstance<TestEvent>();
            e0.Blocking = false;
            e0.Running = true;
            bool e0Ended = false;
            e0.OnEndEvent = () => e0Ended = true;

            var e1 = ScriptableObject.CreateInstance<TestEvent>();
            e0.NextEvent = e1;
            bool e1Started = false;
            e1.OnStartEvent = () => e1Started = true;

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e0 };
            m.Start();
            m.Update();

            Assert.IsFalse(e0Ended);
            Assert.IsTrue(e1Started);
        }

        /// <summary>
        /// Test instant completion of a blocking event.
        /// </summary>
        public void TestInstantCompletionBlocking()
        {
            bool ended = false;
            var e = ScriptableObject.CreateInstance<TestEvent>();
            e.Running = true;
            e.OnEndEvent = () => ended = true;

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e };

            m.Start();
            m.Update();
            Assert.IsFalse(ended);
            m.InstantComplete();
            m.Update();
            Assert.IsTrue(ended);
        }

        /// <summary>
        /// Test instant completion of a non blocking event.
        /// </summary>
        public void TestInstantCompletionNonBlocking()
        {
            bool ended = false;
            var e = ScriptableObject.CreateInstance<TestEvent>();
            e.Running = true;
            e.Blocking = false;
            e.OnEndEvent = () => ended = true;

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e };

            m.Start();
            m.Update();
            Assert.IsFalse(ended);
            m.InstantComplete();
            m.Update();
            Assert.IsTrue(ended);
        }

        public void TestEventHeadCount()
        {
            var e = ScriptableObject.CreateInstance<TestEvent>();

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e };

            Assert.AreEqual(0, m.HeadEventCount());
            m.Start();
            Assert.AreEqual(1, m.HeadEventCount());
        }

        public void TestTotalEventCount()
        {
            var e = ScriptableObject.CreateInstance<TestEvent>();

            var obj = new GameObject();
            var m = obj.AddComponent<FibreManager>();
            m.FirstEvents = new List<FibreEvent> { e };

            Assert.AreEqual(0, m.TotalEventCount());
            m.Start();
            Assert.AreEqual(1, m.TotalEventCount());
        }
        #endregion

        #region LINEAR_EVENT_TESTS
        /// <summary>
        /// Test that LinearEvent.GetNextEvent returns the correct event.
        /// </summary>
        public void TestLinearEventNextEvent()
        {
            var e = ScriptableObject.CreateInstance<LinearEvent>();
            var next = ScriptableObject.CreateInstance<FibreEvent>();
            e.NextEvent = next;

            Assert.AreEqual(e.GetNextEvent(), next);
        }
        #endregion

        #region SLEEP_EVENT_TESTS
        /// <summary>
        /// Test SleepEvent instant completion.
        /// </summary>
        public void TestSleepEventInstantComplete()
        {
            var e = ScriptableObject.CreateInstance<SleepEvent>();
            e.Duration = 1;

            Assert.IsTrue(e.IsInProgress());
            e.InstantComplete();
            Assert.IsFalse(e.IsInProgress());
        }

        /// <summary>
        /// Test that SleepEvent.HasDurationPassed returns true when the duration has passed.
        /// </summary>
        public void TestSleepEventHasDurationPassedTrue()
        {
            double start = 0;
            double dur = 1;
            double time = 2;
            Assert.IsTrue(SleepEvent.HasDurationPassed(start, dur, time));
        }

        /// <summary>
        /// Test that SleepEvent.HasDurationPassed returns false when the duration has not passed.
        /// </summary>
        public void TestSleepEventHasDurationPassedFalse()
        {
            double start = 0;
            double dur = 2;
            double time = 1;
            Assert.IsFalse(SleepEvent.HasDurationPassed(start, dur, time));
        }

        /// <summary>
        /// Test that SleepEvent.HasDurationPassed returns true when exactly the duration has passed.
        /// </summary>
        public void TestSleepEventHasDurationPassedEqual()
        {
            double start = 0;
            double dur = 1;
            double time = 1;
            Assert.IsTrue(SleepEvent.HasDurationPassed(start, dur, time));
        }
        #endregion

        #region EVENT_RENAMER_TESTS
        /// <summary>
        /// Test finding the index of the first digit of a number in a string.
        /// </summary>
        public void TestFindNameNumPrefix()
        {
            Assert.AreEqual(EventRenamerWindow.FindFirstNum("1_abc").Item1, 0);
            Assert.AreEqual(EventRenamerWindow.FindFirstNum("abc_1_abc").Item1, 4);
        }

        /// <summary>
        /// Test finding the length of a number in a string.
        /// </summary>
        public void TestFindNameNumLen()
        {
            Assert.AreEqual(EventRenamerWindow.FindFirstNum("123_abc").Item2, 3);
        }

        /// <summary>
        /// Test attempting to the find number in a string when it does not exist.
        /// </summary>
        public void TestFindNameNumNone()
        {
            (int i, int len) = EventRenamerWindow.FindFirstNum("abc");
            Assert.AreEqual(i, -1);
            Assert.AreEqual(len, -1);
        }

        /// <summary>
        /// Test bumping a number in a string.
        /// </summary>
        public void TestBumpNumInStr()
        {
            Assert.AreEqual(EventRenamerWindow.BumpNumInStr("12_abc", 1, 0), "13_abc");
            Assert.AreEqual(EventRenamerWindow.BumpNumInStr("abc_12_abc", 1, 0), "abc_13_abc");
            Assert.AreEqual(EventRenamerWindow.BumpNumInStr("abc", 1, 0), "abc");
            Assert.AreEqual(EventRenamerWindow.BumpNumInStr("0_abc", -1, 0), "-1_abc");
        }

        /// <summary>
        /// Test setting the min length when bumping a number in a string.
        /// </summary>
        public void TestBumpNumInStrMinLen()
        {
            Assert.AreEqual(EventRenamerWindow.BumpNumInStr("1_abc", 0, 2), "01_abc");
        }

        /// <summary>
        /// Test bumping a negative number in a string.
        /// </summary>
        public void TestBumpNumInStrNeg()
        {
            Assert.AreEqual(EventRenamerWindow.BumpNumInStr("-1_abc", -1, 0), "-2_abc");
        }

        /// <summary>
        /// Test bumping numbers in a list of asset paths.
        /// </summary>
        public void TestBumpEventAssetNums()
        {
            var names = new List<string> { "Assets0/0_Asset", "Assets0/1_Asset", };
            void Rename(string from, string to)
            {
                Assert.IsTrue(names.Exists(s => s == from));
                names[names.FindIndex(s => s == from)] = "Assets0/" + to;
            }
            EventRenamerWindow.BumpEventAssetNums(1, 0, names.ToArray(), Rename);
            Assert.AreEqual(names[0], "Assets0/1_Asset");
            Assert.AreEqual(names[1], "Assets0/2_Asset");
        }

        /// <summary>
        /// Test that bumping asset paths up doesn't result in duplicate names.
        /// </summary>
        public void TestBumpEventAssetNumsNoDuplicateUp()
        {
            var names = new List<string> { "0_Asset", "1_Asset", };
            void Rename(string from, string to)
            {
                Assert.IsFalse(names.Exists(s => s == to));
                names[names.FindIndex(s => s == from)] = to;
            }
            EventRenamerWindow.BumpEventAssetNums(1, 0, names.ToArray(), Rename);
        }

        /// <summary>
        /// Test that bumping asset paths down doesn't result in duplicate names.
        /// </summary>
        public void TestBumpEventAssetNumsNoDuplicateDown()
        {
            var names = new List<string> { "0_Asset", "1_Asset", };
            void Rename(string from, string to)
            {
                Assert.IsFalse(names.Exists(s => s == to));
                names[names.FindIndex(s => s == from)] = to;
            }
            EventRenamerWindow.BumpEventAssetNums(-1, 0, names.ToArray(), Rename);
        }
        #endregion
    }
}
#endif
