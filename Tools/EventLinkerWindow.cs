#if UNITY_EDITOR
using System.Linq;
using UnityEditor;

namespace Fibre
{
    /// <summary>
    /// A tool for automatically linking a list of `LinearEvent`s.
    /// </summary>
    public static class EventLinker
    {
        /// <summary>
        /// Link linear events.
        /// </summary>
        [MenuItem("Assets/Fibre/Link Linear Events")]
        public static void LinkLinearEvents()
        {
            // Sort events by their names.
            var selection = Selection.objects.Select(obj => obj as LinearEvent).ToList();
            selection.Sort((lhs, rhs) => lhs.name.CompareTo(rhs.name));

            // Link each event to the next event.
            for (int i = 0; i < selection.Count - 1; ++i)
            {
                LinkToNext(selection[i], selection[i + 1]);
            }
            FibreDebug.Log($"Linked {selection.Count} events.");
        }

        /// <summary>
        /// Check if linking can be run on the current asset selection.
        /// </summary>
        /// <returns></returns>
        [MenuItem("Assets/Fibre/Link Linear Events", true)]
        public static bool ValidateBumpEventNumsSelection()
        {
            return Selection.objects.All(obj => obj is LinearEvent);
        }

        /// <summary>
        /// Link `current` event to `next`.
        /// </summary>
        /// <param name="current"></param>
        /// <param name="next"></param>
        private static void LinkToNext(LinearEvent current, LinearEvent next)
        {
            var serializedObject = new SerializedObject(current);
            var property = serializedObject.FindProperty(nameof(current.NextEvent));
            property.objectReferenceValue = next;
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif
