#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Fibre
{
    /// <summary>
    /// A tool for bumping the numbered names of a list of event assets.
    /// </summary>
    public class EventRenamerWindow : EditorWindow
    {
        public int Bump = 1;
        private uint _minDigits = 2;

        private UnityEngine.Object[] _selection = new UnityEngine.Object[0];
        private Vector2 _previewScroll = Vector2.zero;

        /// <summary>
        /// Open the renamer window.
        /// </summary>
        [MenuItem("Assets/Fibre/Bump Event Numbers")]
        public static void OpenRenamerWindow()
        {
            var win = GetWindow<EventRenamerWindow>();
            win._selection = Selection.objects;
            Array.Sort(win._selection, (a, b) => a.name.CompareTo(b.name));
        }

        /// <summary>
        /// Check if renaming can be run on the current asset selection.
        /// </summary>
        /// <returns></returns>
        [MenuItem("Assets/Fibre/Bump Event Numbers", true)]
        public static bool ValidateBumpEventNumsSelection()
        {
            return Selection.objects.All(obj => obj is FibreEvent);
        }

        private void OnGUI()
        {
            Bump = EditorGUILayout.IntField(nameof(Bump), Bump);
            _minDigits = (uint)EditorGUILayout.IntSlider("Min Digits", (int)_minDigits, 1, 3);

            if (GUILayout.Button("Bump Event Numbers"))
            {
                BumpSelection();
            }

            // Show preview of new names.
            _previewScroll = GUILayout.BeginScrollView(_previewScroll);
            GUILayout.BeginVertical(GUI.skin.box);
            var centeredLabel = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.UpperCenter };
            foreach (UnityEngine.Object obj in _selection)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(obj.name, centeredLabel);
                GUILayout.Label("->", centeredLabel);
                GUILayout.Label(BumpNumInStr(obj.name, Bump, _minDigits), centeredLabel);
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
            GUILayout.EndScrollView();
        }

        /// <summary>
        /// Bump the numbers of the selected event assets.
        /// </summary>
        private void BumpSelection()
        {
            var paths = new List<string>();
            foreach (UnityEngine.Object obj in _selection)
            {
                paths.Add(AssetDatabase.GetAssetPath(obj));
            }
            static void RenameAsset(string a, string b) => AssetDatabase.RenameAsset(a, b);
            BumpEventAssetNums(Bump, _minDigits, paths.ToArray(), RenameAsset);
        }

        /// <summary>
        /// Find the index and length of the first number in a string.
        /// </summary>
        /// <param name="str"></param>
        /// <returns>
        /// (Start index, Length), or (-1, -1) if no number is found.
        /// </returns>
        public static (int, int) FindFirstNum(string str)
        {
            Match m = Regex.Match(str, "-?\\d+");
            return m.Success ? (m.Index, m.Length) : (-1, -1);
        }

        /// <summary>
        /// Bump a number in a string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="bump"></param>
        /// <param name="minLen"></param>
        /// <returns>
        /// The new string. The string will be unchanged if it contained
        /// no numbers.
        /// </returns>
        public static string BumpNumInStr(string str, int bump, uint minLen)
        {
            // Get the num from the string.
            (int i, int len) = FindFirstNum(str);
            if (i == -1 || len == -1)
            {
                return str;
            }
            int num = int.Parse(str.Substring(i, len));

            // Replace the num in the string.
            string bumped = (num + bump).ToString($"D{minLen}");
            return str[..i] + bumped + str[(i + len)..];
        }

        /// <summary>
        /// Bump the numbered names of a list of event assets.
        /// </summary>
        /// <param name="bump"></param>
        /// <param name="minDigits"></param>
        /// <param name="paths"></param>
        /// <param name="fn">A function that renames an asset.</param>
        public static void BumpEventAssetNums(
            int bump,
            uint minDigits,
            string[] paths,
            Action<string, string> fn
        )
        {
            var ord = bump < 0 ? paths : paths.Reverse();
            foreach (string path in ord)
            {
                string name = Path.GetFileName(path);
                string bumped = BumpNumInStr(name, bump, minDigits);
                fn(path, bumped);
            }
        }
    }
}
#endif
