using System;

namespace Fibre
{
    /// <summary>
    /// A wrapper around a FibreEvent that provides more error checking and convenience.
    /// </summary>
    public class RunningEvent
    {
        private readonly FibreEvent _event = null;
        private bool _ended = false;

        /// <summary>
        /// Construct a RunningEvent with a clone of a FibreEvent.
        /// </summary>
        /// <param name="original">The original event to be cloned.</param>
        public RunningEvent(FibreEvent original)
        {
            if (original)
            {
                _event = original.Clone();
            }
        }

        public bool Blocking
        {
            get { return _event.Blocking; }
        }

        /// <summary>
        /// Try to start the event.
        /// </summary>
        /// <returns>True on success.</returns>
        public bool TryStartEvent()
        {
            if (_event == null)
            {
                return false;
            }

            try
            {
                _event.StartEvent();
            }
            catch (Exception e)
            {
                FibreDebug.LogException($"Failed to start event \"{_event.name}\":", e);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Try to check if the event is in progress.
        /// </summary>
        /// <param name="inProgress"></param>
        /// <returns>True on success.</returns>
        public bool TryIsInProgress(out bool inProgress)
        {
            inProgress = false;
            if (_event == null)
            {
                return false;
            }

            try
            {
                inProgress = _event.IsInProgress();
            }
            catch (Exception e)
            {
                FibreDebug.LogException($"Failed check progress of event \"{_event.name}\":", e);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Try to end the event.
        /// No effect if the event is already ended.
        /// </summary>
        /// <returns>True on success.</returns>
        public bool TryEndEvent()
        {
            if (_ended)
            {
                return true;
            }
            _ended = true;
            if (_event == null)
            {
                return false;
            }

            try
            {
                _event.OnEventEnd();
            }
            catch (Exception e)
            {
                FibreDebug.LogException($"Failed to end event \"{_event.name}\":", e);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Try to get the next event.
        /// </summary>
        /// <param name="next"></param>
        /// <returns>True on success.</returns>
        public bool TryGetNextEvent(out RunningEvent nextEvent)
        {
            nextEvent = null;
            if (!_event)
            {
                return false;
            }

            FibreEvent fibreEvent;
            try
            {
                fibreEvent = _event.GetNextEvent();
            }
            catch (Exception e)
            {
                FibreDebug.LogException($"Failed to get next event of \"{_event.name}\":", e);
                return false;
            }

            if (fibreEvent != null)
            {
                nextEvent = new RunningEvent(fibreEvent);
            }
            return true;
        }

        /// <summary>
        /// Try to instantly complete the event.
        /// </summary>
        /// <returns>True on success.</returns>
        public bool TryInstantComplete()
        {
            if (_event == null)
            {
                return false;
            }

            try
            {
                _event.InstantComplete();
            }
            catch (Exception e)
            {
                FibreDebug.LogException($"Failed to instant complete event \"{_event.name}\":", e);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Try to check if the manager should continue on from this event.
        /// </summary>
        /// <param name="shouldContinue"></param>
        /// <returns>True on success.</returns>
        public bool TryShouldContinue(out bool shouldContinue)
        {
            if (!_event)
            {
                shouldContinue = false;
                return false;
            }

            if (!_event.Blocking)
            {
                shouldContinue = true;
                return true;
            }

            bool success = TryIsInProgress(out bool inProgress);
            shouldContinue = !inProgress;
            return success;
        }
    }
}
