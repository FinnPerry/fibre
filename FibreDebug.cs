using System;
using UnityEngine;

namespace Fibre
{
    /// <summary>
    /// Functions for logging messages with a Fibre prefix.
    /// </summary>
    public static class FibreDebug
    {
        /// <summary>
        /// Log a message to the Unity console.
        /// </summary>
        /// <param name="message"></param>
        public static void Log(string message)
        {
            Debug.Log(MakeLogMessage(message));
        }

        /// <summary>
        /// Log a warning message to the Unity console.
        /// </summary>
        /// <param name="message"></param>
        public static void LogWarning(string message)
        {
            Debug.LogWarning(MakeLogMessage(message));
        }

        /// <summary>
        /// Log an error message to the Unity console.
        /// </summary>
        /// <param name="message"></param>
        public static void LogError(string message)
        {
            Debug.LogError(MakeLogMessage(message));
        }

        /// <summary>
        /// Log an exception to the Unity console.
        /// </summary>
        /// <param name="message"></param>
        public static void LogException(string message, Exception err)
        {
            LogError($"{message}\nError: {err.Message}{err.StackTrace}");
        }

        /// <summary>
        /// Add the Fibre prefix to a log message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string MakeLogMessage(string message)
        {
            return $"Fibre: {message}";
        }
    }
}
