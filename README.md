# Fibre

A system for running event sequences in Unity.

# Installation

To install Fibre into your Unity project, simply place it in your "Assets" folder.

For example cloning with git: `git clone https://codeberg.org/NoraPerry/fibre.git MyProject/Assets/Fibre`

# Usage

This tutorial will cover the basic usage. You can also check out an example scene and event
sequence at `Examples/FibreExample.unity`.

## Fibre Manager

The Fibre Manager is the component that handles running the event sequences.

To get setup, add the `FibreManager` component to a GameObject in your scene.

![AddFibreManagerComponent](Resources/AddFibreManagerComponent.png)

## Creating Events

You can create events via the "Create" menu under "Fibre".

![CreateEvent](Resources/CreateEvent.png)

We'll create a `DebugLogEvent` and call it `0_SayHello`. You can name events however you like,
however prefixing them with a number is recommended so your events are listed in the correct
order when sorted alphabetically. Fibre also provides some tooling to help deal with numbered events.

A `DebugLogEvent` will log a message to the Unity console when run. We'll fill in the message field to say "Hello!".

![CreatedSayHelloEvent](Resources/CreatedSayHelloEvent.png)

## Running Events

To run the event, the manager needs to know about it. Select the `FibreManager` object in the
inspector and add a new entry to the `FirstEvents` array. We'll add our `0_SayHello` event.

![RunSayHelloEvent](Resources/RunSayHelloEvent.png)

Now when we play our project, our event is run and "Hello!" is logged to the console.

![LoggedHello](Resources/LoggedHello.png)

## Adding More Events

Running 1 event is great, but we want to run an event *sequence*.

We'll create 2 more events:

- `1_Sleep`: a `SleepEvent` which will pause the event sequence for 1 second.
- `2_SayWorld`: a second `DebugLogEvent` which will log "World!".

![CreatedSleepEvent](Resources/CreatedSleepEvent.png)
![CreatedSayWorldEvent](Resources/CreatedSayWorldEvent.png)

To make these events run in a sequence, we need to assign their `NextEvent` fields in the inspector.
We'll assign `0_SayHello`'s next event to `1_Sleep`, and `1_Sleep`'s next event to `2_SayWorld`.
There's no events after `2_SayWorld` so we can leave it's field blank.

![SetNextEvents](Resources/SetNextEvents.png)

Now when we play our project, "Hello!" is logged to the console, then 1 second later "World!" is logged.

![LoggedHelloWorld](Resources/LoggedHelloWorld.png)

Events can only have 1 `NextEvent` that comes after them in the sequence, however custom event types
can be implemented with branching logic so multiple events are specified, then at runtime it picks
which one to run based on a condition.

It is also possible to turn our event sequence into a loop by for example setting `2_SayWorld`'s
next event to `0_SayHello`. Then if we press play it will continuously log "Hello!", "World!", "Hello!", etc.

## Instant Completion

When developing, it can be useful to jump through the event sequence to get the the part you want
to test more quickly. To do this, you can call the `.InstantComplete()` method on the manager.

This signals to all the currently running events that they should instantly complete, however it is
not guaranteed that all events actually comply. It's up to each event's implementation to
correctly handle the completion. If they don't then the manager will simply continue to run them
like normal.

For example:

```c-sharp
using UnityEngine;
using Fibre;

public class InstantCompleteOnSpace : MonoBehaviour
{
    public FibreManager Manager = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Manager.InstantComplete();
        }
    }
}
```

## Blocking Events

The `Blocking` toggle on an event specified whether or not the event should block the event sequence.
When toggled on, the event will run to completion before the sequence continues.
When toggled off, the sequence will continue on to the next event while the non-blocking event
is still running in the background.
For example if we toggle our `1_Sleep` event to be non-blocking, then the 1 second delay between logging
"Hello!" and "World!" will disappear. `1_Sleep` still runs but has no visible effects.

![SleepNonBlocking](Resources/SleepNonBlocking.png)

## Bumping Event Numbers

If you number your events, it can be a pain to insert or remove an event from the middle of the
sequence as you have to adjust all the numbers. Fibre provides a tool to more easily bump the
numbers up or down.

To access it, select the events you want to affect then right click them and go to
"Fibre/Bump Numbered Events".

![OpenEventNumberBumper](Resources/OpenEventNumberBumper.png)

Once the tool is open you can specify how much to increment or decrement the event numbers,
and set the amount of digits.

![BumpEventNumbers](Resources/BumpEventNumbers.png)

Once you've input your settings, click "Bump Event Numbers" to apply the change.

## Automatically Linking Events

When moving events around you'll often need to re-assign the `NextEvent` fields.
To speed this up, Fibre provides a tool to automatically link a selection of events.

To access it, select the events you want to affect then right click them and go to
"Fibre/Link Linear Events". This links all the events in alphabetical order.

![LinkLinearEvents](Resources/LinkLinearEvents.png)

The linker tool is only accessible if your selection only contains instances or derivatives of
`LinearEvent`.

## Custom Event Types

Fibre provides a few basic event types, however you'll most likely want to create custom event
types for your applications needs. As an example, we'll create an event type that moves a cube
to a specified location.

Our example event type won't cover *all* the methods you can implement for custom event types.
For a full list, see the source code for the base event type at `EventTypes/FibreEvent.cs`.

We'll create a new script at `Assets/Scripts/MoveCubeEvent.cs`:

```c-sharp
using UnityEngine;
using Fibre;

[CreateAssetMenu(menuName = "Fibre/MyEvents/MoveCubeEvent")]
public class MoveCubeEvent : FibreEvent
{
}
```

This is the bare bones of an event type. We create a class that derives from
`FibreEvent`, and add the `CreateAssetMenu` attribute so we can create instances
of our event via the the create menu.

Like our `DebugLog` and `Sleep` events from before, we want to specify the next event to run
after our `MoveCubeEvent`. The easiest way to do this is to change our class to derive from
`LinearEvent` which provides the `NextEvent` field and implements the necessary logic.

```c-sharp
// ...

public class MoveCubeEvent : LinearEvent

// ...
```

Since Fibre events are `ScriptableObject`s, they don't directly run on a GameObject.
The actual logic for moving the cube will be implemented in a `MonoBehaviour` script
that the event will interface with.

We'll create this script at `Assets/Scripts/CubeMover.cs`, and create the cube GameObject
in the scene with the `CubeMover` component attached.

```c-sharp
using UnityEngine;

public class CubeMover : MonoBehaviour
{
    public Vector3 TargetPosition = Vector3.zero;
    public float MovementSpeed = 1f;

    private void Update()
    {
        float maxDelta = MovementSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, TargetPosition, maxDelta);
    }
}
```

![CreatedTestCube](Resources/CreatedTestCube.png)

### `StartEvent`

The `StartEvent` function is called when an event is started, similar to `Start` on a `MonoBehaviour`.
We'll use it to get a reference to the cube object and set it's target position.

```c-sharp
// ...

public class MoveCubeEvent : LinearEvent
{
    public Vector3 TargetPosition = Vector3.zero;
    private CubeMover _cubeMover = null;

    public override void StartEvent()
    {
        base.StartEvent();
        GameObject cubeObject = GameObject.Find("TestCube");
        _cubeMover = cubeObject.GetComponent<CubeMover>();
        _cubeMover.TargetPosition = TargetPosition;
    }
}
```

Now if we create an instance of our event (and add it to the sequence), when we play the project
the cube will be moved to the target position.

![CreatedMoveCubeRightEvent](Resources/CreatedMoveCubeRightEvent.png)

### `IsInProgress`

At the moment, our event is considered complete immediately by the manager. When our event is
set to "Blocking", we want it to wait until the cube has reached the target position before
continuing the event sequence.

The `FibreManager` determines whether an event is complete or still running by calling
`.IsInProgress()` on it. We can implement this for our event by checking the cube's position.

It is generally good practise to `||` your result with `base.IsInProgress()` to make sure
that the event doesn't end early and cancel something that the base event type was doing.

```c-sharp
// ...

public class MoveCubeEvent : LinearEvent
{
    // ...
    
    public override bool IsInProgress()
    {
        bool hasReachedTarget = _cubeMover.transform.position == TargetPosition;
        return !hasReachedTarget || base.IsInProgress();
    }
}
```

### `InstantComplete`

As mentioned in the "Instant Completion" section, it's up the each event type to correctly handle
instant completion. At the moment our event will simply ignore any attempts at instantly
completing it. We can fix this by implementing `InstantComplete` and setting the cube's position
to the target.

When properly implemented, `InstantComplete` should put the event in a state where the next call to
`IsInProgress` returns false.

```c-sharp
// ...

public class MoveCubeEvent : LinearEvent
{
    // ...
    
    public override void InstantComplete()
    {
        base.InstantComplete();
        _cubeMover.transform.position = TargetPosition;
    }
}
```
